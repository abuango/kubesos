# KubeSOS

`kubectl` and `helm` wrapper to retrieve GitLab cluster configuration and logs from GitLab Cloud Native chart deployments

#### Usage

Download and execute:

```
chmod +x kubeSOS.sh
./kubeSOS.sh [NAMESPACE]
``` 

Or use `curl`:

```
curl https://gitlab.com/gitlab-com/support/toolbox/kubesos/raw/master/kubeSOS.sh [NAMESPACE] | bash
``` 

Data will be archived to `kubesos-<timestamp>.tar.gz`
