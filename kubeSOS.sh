#!/bin/bash

case "$1" in
  -n | --namespace)
     namespace="-n $2"
     ;;
  -h | --help)
      echo "Usage: $0 [NAMESPACE]" >&2
      exit 1
      ;;
   "" )
      echo "Using default namespace"
      ;;
esac

timestamp="$(date +%d%m%Y%H%M%S)"
archive_dir="./kubesos-$timestamp"
mkdir -p $archive_dir


# Verify requirements and write versions to file
echo "Getting kubectl version..."
kubectl_version="$(kubectl version 2> /dev/null)"

if [ -n "$kubectl_version" ]; then
    echo "$kubectl_version" > $archive_dir/kubectl-version 
  else
    echo -e "Could not load kubectl version, do you have kubectl installed?\n"
    exit 1
fi

echo "Getting helm version..."
helm_version="$(helm version 2> /dev/null)"

if [ -n "$helm_version" ]; then
    echo "$helm_version" > $archive_dir/helm-version 
  else
    echo -e "Could not load helm version, do you have helm installed?\n"
fi

echo "Getting GitLab chart version..."
chart_version="$(helm ls 2> /dev/null)"

if [ -n "$chart_version" ]; then
    echo "$chart_version" > $archive_dir/chart-version 
  else
    echo -e "Unable to retrieve chart version\n"
fi


echo "Get pods..."
kubectl get pods $namespace > $archive_dir/pods
echo "Describe pods..."
kubectl describe pods $namespace > $archive_dir/describe
echo "Describe nodes..."
kubectl describe nodes $namespace > $archive_dir/nodes

# We could dynamically load these
app_name=(
         gitaly
         gitlab-gitlab-runner
         gitlab-shell
         migrations
         minio
         nginx-ingress
         postgresql
         prometheus
         redis
         registry
         sidekiq
         task-runner
         unicorn
         webhook
)

# Retrieve logs for each component in array
for app in "${app_name[@]}"
do
  echo "Retrieve logs for $app..."
  kubectl logs $namespace -l app=$app --all-containers=true --max-log-requests=50 --tail=10000 > $archive_dir/$app.log
done

archive_file="$archive_dir.tar.gz"
tar -czf $archive_file $archive_dir
echo "Archive file $archive_file created."
